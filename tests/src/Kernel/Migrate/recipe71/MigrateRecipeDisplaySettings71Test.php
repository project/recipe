<?php

namespace Drupal\Tests\recipe\Kernel\Migrate\recipe71;

use Drupal\Core\Entity\Entity\EntityViewDisplay;

/**
 * Tests migration of Recipe 7.x-1.x ingredient variables to configuration.
 *
 * @group recipe
 */
class MigrateRecipeDisplaySettings71Test extends MigrateRecipe71TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ingredient', 'node', 'rdf', 'recipe', 'text'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('ingredient');
    $this->installEntitySchema('node');
    $this->installConfig(static::$modules);
    $this->executeMigrations(['recipe1x_ingredient_field_display']);
  }

  /**
   * Tests migration of ingredient field instance variables.
   */
  public function testMigration() {
    $display = EntityViewDisplay::load('node.recipe.default');
    $field_component = $display->getComponent('recipe_ingredient');
    $this->assertSame('{%d }%d/%d', $field_component['settings']['fraction_format']);
    $this->assertSame(1, $field_component['settings']['unit_display']);
  }

}
