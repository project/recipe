<?php

namespace Drupal\Tests\recipe\Kernel\Migrate\recipe71;

use Drupal\Core\Database\Database;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * @covers \Drupal\recipe\Plugin\migrate\source\recipe71\Recipe71
 * @group recipe
 */
class MigrateRecipe71Test extends MigrateRecipe71TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_translation',
    'ingredient',
    'language',
    'menu_ui',
    'node',
    'rdf',
    'recipe',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('ingredient');
    $this->installEntitySchema('node');
    $this->installConfig(static::$modules);
    $this->installSchema('node', ['node_access']);
  }

  /**
   * Asserts various aspects of a recipe.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The imported recipe node.
   * @param object $recipe
   *   A stdClass object with the following properties:
   *   - $recipe->description: A short description of the recipe.
   *   - $recipe->ingredients: An array of ingredients for the recipe.
   *   - $recipe->instructions: Instructions on how to prepare the recipe.
   *   - $recipe->notes: Other notes about this recipe.
   *   - $recipe->preptime: The preparation time in minutes.
   *   - $recipe->source: Who deserves credit for this recipe.
   */
  protected function assertRecipeFields(NodeInterface $node, \stdClass $recipe) {
    $this->assertEquals($recipe->cooktime, $node->recipe_cook_time->value);
    $this->assertSame($recipe->description, $node->recipe_description->value);
    $this->assertSame($recipe->instructions, $node->recipe_instructions->value);
    $this->assertSame($recipe->notes, $node->recipe_notes->value);
    $this->assertEquals($recipe->preptime, $node->recipe_prep_time->value);
    $this->assertSame($recipe->source, $node->recipe_source->value);

    for ($i = 0; $i < count($recipe->ingredients); $i++) {
      $this->assertEquals($recipe->ingredients[$i]->quantity, $node->recipe_ingredient[$i]->quantity);
      $this->assertEquals($recipe->ingredients[$i]->unit_key, $node->recipe_ingredient[$i]->unit_key);
      $this->assertEquals($recipe->ingredients[$i]->ingredient_id, $node->recipe_ingredient[$i]->target_id);
      $this->assertEquals($recipe->ingredients[$i]->note, $node->recipe_ingredient[$i]->note);
    }
  }

  /**
   * Tests the migration of Recipe 7.x fields except for the yield fields.
   *
   * The yield fields are handled by a separate migration which is tested by
   * another class.
   *
   * @dataProvider providerTestRecipe
   */
  public function testRecipeFields(array $migrations) {
    $this->executeMigrations([
      'language',
      'd7_user_role',
      'd7_user',
      'd7_node_type',
      'recipe1x_ingredient',
    ]);
    $this->executeMigrations($migrations);
    $database_connection = Database::getConnection('default', 'migrate');

    $recipes = $database_connection
      ->select('recipe', 'r')
      ->fields('r')
      ->execute()
      ->fetchAll();

    foreach ($recipes as &$source) {
      // Get a list of ingredient IDs from the old database.
      $source->ingredients = $database_connection->select('recipe_node_ingredient', 'rni')
        ->fields('rni')
        ->condition('rni.nid', $source->nid)
        ->execute()
        ->fetchAll();
    }

    $node = Node::load(1);
    $this->assertRecipeFields($node, array_shift($recipes));

    // Verify the fields of an English-language recipe.
    $node = Node::load(2);
    $this->assertRecipeFields($node, array_shift($recipes));
    $this->assertTrue($node->hasTranslation('is'), 'Node 2 has an Islandic translation');

    // Verify the fields of a Islandic-language recipe that was translated from
    // node 2.
    $translation = $node->getTranslation('is');
    $this->assertRecipeFields($translation, array_shift($recipes));
  }

  /**
   * Tests node migration.
   */
  public static function providerTestRecipe(): array {
    return [
      'obsolete node migrations' => [
        'migrations' => [
          'd7_node',
          'd7_node_translation',
        ],
      ],
      'complete node migrations' => [
        'migrations' => [
          'd7_node_complete',
        ],
      ],
    ];
  }

}
