<?php

namespace Drupal\Tests\recipe\Kernel\Migrate\recipe71;

use Drupal\field\Entity\FieldConfig;

/**
 * Tests migration of Recipe 7.x-1.x ingredient variables to configuration.
 *
 * @group recipe
 */
class MigrateRecipeFieldSettings71Test extends MigrateRecipe71TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ingredient', 'node', 'rdf', 'recipe', 'text'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('ingredient');
    $this->installEntitySchema('node');
    $this->installConfig(static::$modules);
    $this->executeMigrations(['recipe1x_ingredient_field_instance']);
  }

  /**
   * Tests migration of ingredient field instance variables.
   */
  public function testMigration() {
    $field_instance = FieldConfig::load('node.recipe.recipe_ingredient');
    $this->assertSame($field_instance->getSetting('default_unit'), 'cup');
  }

}
