<?php

namespace Drupal\Tests\recipe\Kernel\Migrate\recipe72;

use Drupal\node\Entity\Node;
use Drupal\Tests\migrate_drupal\Kernel\d7\MigrateDrupal7TestBase;

/**
 * @covers \Drupal\recipe\Plugin\migrate\source\Recipe72
 * @group recipe
 */
class MigrateRecipe72Test extends MigrateDrupal7TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ingredient',
    'menu_ui',
    'node',
    'rdf',
    'recipe',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getFixtureFilePath() {
    return __DIR__ . '/../../../../fixtures/recipe72.php';
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('node');
    $this->installConfig(static::$modules);
    $this->installSchema('node', ['node_access']);
    $this->executeMigrations([
      'd7_user_role',
      'd7_user',
      'd7_node_type',
      'd7_node',
      'recipe72_recipe',
    ]);
  }

  /**
   * Tests the migration of Recipe 7.x yield fields.
   */
  public function testRecipeFields() {
    $node = Node::load(1);
    $this->assertEquals('1', $node->recipe_yield_amount->value);
    $this->assertSame('Glass', $node->recipe_yield_unit->value);
  }

}
