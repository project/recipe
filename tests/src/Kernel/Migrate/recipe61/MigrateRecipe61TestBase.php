<?php

namespace Drupal\Tests\recipe\Kernel\Migrate\recipe61;

use Drupal\Tests\migrate_drupal\Kernel\d6\MigrateDrupal6TestBase;

/**
 * Provides a base class for Recipe migrations from Recipe 6.x-1.x.
 */
abstract class MigrateRecipe61TestBase extends MigrateDrupal6TestBase {

  /**
   * {@inheritdoc}
   */
  protected function getFixtureFilePath() {
    return __DIR__ . '/../../../../fixtures/recipe61.php';
  }

}
