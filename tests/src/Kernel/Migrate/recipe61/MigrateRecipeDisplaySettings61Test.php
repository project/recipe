<?php

namespace Drupal\Tests\recipe\Kernel\Migrate\recipe61;

use Drupal\Core\Entity\Entity\EntityViewDisplay;

/**
 * Tests migration of Recipe 6.x-1.x ingredient variables to configuration.
 *
 * @group recipe
 */
class MigrateRecipeDisplaySettings61Test extends MigrateRecipe61TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ingredient', 'node', 'rdf', 'recipe', 'text'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('ingredient');
    $this->installEntitySchema('node');
    $this->installConfig(static::$modules);
    $this->executeMigrations(['recipe1x_ingredient_field_display']);
  }

  /**
   * Tests migration of ingredient field instance variables.
   */
  public function testMigration() {
    $display = EntityViewDisplay::load('node.recipe.default');
    $field_component = $display->getComponent('recipe_ingredient');
    $this->assertSame($field_component['settings']['fraction_format'], '{%d }%d/%d');
    $this->assertSame($field_component['settings']['unit_display'], 1);
  }

}
