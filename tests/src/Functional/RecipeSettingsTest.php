<?php

namespace Drupal\Tests\recipe\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;

/**
 * @covers recipe_form_node_type_edit_form_alter
 * @group recipe
 */
class RecipeSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['recipe'];

  /**
   * A test user with administrative privileges.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create and log in the admin user with Recipe content permissions.
    $permissions = [
      'create recipe content',
      'edit any recipe content',
      'administer content types',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests the pseudo-field label settings.
   */
  public function testPseudoFieldLabels() {
    $node_type = NodeType::load('recipe');
    $title = $this->randomMachineName(16);
    $yield_amount = 5;
    $yield_unit = $this->randomMachineName(10);
    $preptime = 60;
    $cooktime = 135;

    $edit = [
      'title[0][value]' => $title,
      'recipe_yield_amount[0][value]' => $yield_amount,
      'recipe_yield_unit[0][value]' => $yield_unit,
      'recipe_prep_time[0][value]' => $preptime,
      'recipe_cook_time[0][value]' => $cooktime,
    ];

    // Post the values to the node form.
    $this->drupalGet('node/add/recipe');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains(new FormattableMarkup('Recipe @title has been created.', ['@title' => $title]));

    // Check for the default pseudo-field labels.
    $this->assertSession()->pageTextContains('Total time');
    $this->assertSession()->pageTextContains('Yield');

    // Alter the pseudo-field labels.
    $total_time_label = $this->randomMachineName(20);
    $yield_label = $this->randomMachineName(20);
    $node_type->setThirdPartySetting('recipe', 'total_time_label', $total_time_label)
      ->setThirdPartySetting('recipe', 'yield_label', $yield_label)
      ->save();

    // Check the node display for the new labels.
    $this->drupalGet('node/1');
    $this->assertSession()->pageTextContains($total_time_label);
    $this->assertSession()->pageTextContains($yield_label);

    // Alter the pseudo-field label displays.
    $node_type->setThirdPartySetting('recipe', 'total_time_label_display', 'hidden')
      ->setThirdPartySetting('recipe', 'yield_label_display', 'hidden')
      ->save();

    // Check the node display for the new labels.
    $this->drupalGet('node/1');
    $this->assertSession()->pageTextNotContains($total_time_label);
    $this->assertSession()->pageTextNotContains($yield_label);
  }

}
