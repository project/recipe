<?php

namespace Drupal\Tests\ingredient\Kernel\Migrate\recipe72;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\node\Entity\Node;
use Drupal\Tests\migrate_drupal\Kernel\d7\MigrateDrupal7TestBase;

/**
 * @coversDefaultClass \Drupal\ingredient\Plugin\migrate\field\IngredientReference
 * @group ingredient
 */
class MigrateIngredient72Test extends MigrateDrupal7TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ingredient',
    'menu_ui',
    'node',
    'rdf',
    'recipe',
    'text',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getFixtureFilePath() {
    return __DIR__ . '/../../../../fixtures/ingredient72.php';
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('ingredient');
    $this->installEntitySchema('node');
    $this->installConfig(static::$modules);
    $this->installSchema('node', ['node_access']);
    $this->executeMigrations([
      'd7_user_role',
      'd7_user',
      'd7_node_type',
      'd7_view_modes',
      'd7_field',
      'd7_field_instance',
      'd7_field_instance_widget_settings',
      'd7_field_formatter_settings',
      'd7_node',
    ]);
  }

  /**
   * @covers ::alterFieldInstanceMigration
   * @covers ::alterFieldFormatterMigration
   * @covers ::defineValueProcessPipeline
   */
  public function testRecipeFields() {
    $field_instance = FieldConfig::load('node.recipe.recipe_ingredient');
    $this->assertSame('cup', $field_instance->getSetting('default_unit'));

    $display = EntityViewDisplay::load('node.recipe.default');
    $field_component = $display->getComponent('recipe_ingredient');
    $this->assertSame('{%d }%d/%d', $field_component['settings']['fraction_format']);
    $this->assertSame(1, $field_component['settings']['unit_display']);

    $node = Node::load(1);
    $this->assertEquals('2', $node->recipe_ingredient[0]->quantity);
    $this->assertEquals('cup', $node->recipe_ingredient[0]->unit_key);
    $this->assertEquals('1', $node->recipe_ingredient[0]->target_id);
    $this->assertEquals('cold', $node->recipe_ingredient[0]->note);
    $this->assertEquals('1', $node->recipe_ingredient[1]->quantity);
    $this->assertEquals('tablespoon', $node->recipe_ingredient[1]->unit_key);
    $this->assertEquals('2', $node->recipe_ingredient[1]->target_id);
    $this->assertEquals('', $node->recipe_ingredient[1]->note);
  }

}
