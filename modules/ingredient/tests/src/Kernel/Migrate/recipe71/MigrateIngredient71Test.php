<?php

namespace Drupal\Tests\ingredient\Kernel\Migrate\recipe71;

use Drupal\Core\Database\Database;
use Drupal\ingredient\Entity\Ingredient;
use Drupal\ingredient\IngredientInterface;

/**
 * @covers \Drupal\ingredient\Plugin\migrate\destination\EntityIngredient
 * @covers \Drupal\ingredient\Plugin\migrate\source\recipe1x\Ingredient
 * @group recipe
 */
class MigrateIngredient71Test extends MigrateIngredient71TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ingredient'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('ingredient');
    $this->executeMigrations(['recipe1x_ingredient']);
  }

  /**
   * Asserts various aspects of an ingredient.
   *
   * @param string $id
   *   The ingredient ID.
   * @param string $label
   *   The ingredient name.
   */
  protected function assertEntity($id, $label) {
    /** @var \Drupal\ingredient\IngredientInterface $ingredient */
    $ingredient = Ingredient::load($id);
    $this->assertTrue($ingredient instanceof IngredientInterface);
    $this->assertSame($label, $ingredient->label());
  }

  /**
   * Tests the Drupal 7 ingredient to Drupal 8 migration.
   */
  public function testIngredient() {
    $ingredients = Database::getConnection('default', 'migrate')
      ->select('recipe_ingredient', 'i')
      ->fields('i')
      ->execute()
      ->fetchAll();

    foreach ($ingredients as $source) {
      $this->assertEntity($source->id, $source->name);
    }
  }

}
