<?php

namespace Drupal\Tests\ingredient\Kernel\Migrate\recipe71;

/**
 * Tests migration of Recipe 7.x-1.x ingredient variables to configuration.
 *
 * @group recipe
 */
class MigrateIngredientSettings71Test extends MigrateIngredient71TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ingredient'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(static::$modules);
    $this->executeMigrations(['recipe1x_ingredient_settings']);
  }

  /**
   * Tests migration of ingredient variables to configuration.
   */
  public function testMigration() {
    $config = \Drupal::config('ingredient.settings')->get();
    $this->assertSame(1, $config['ingredient_name_normalize']);
  }

}
