<?php

namespace Drupal\Tests\ingredient\Kernel\Migrate\recipe61;

use Drupal\Tests\migrate_drupal\Kernel\d6\MigrateDrupal6TestBase;

/**
 * Provides a base class for Ingredient migrations from Recipe 6.x-1.5.
 */
abstract class MigrateIngredient61TestBase extends MigrateDrupal6TestBase {

  /**
   * {@inheritdoc}
   */
  protected function getFixtureFilePath() {
    return __DIR__ . '/../../../../fixtures/ingredient615.php';
  }

}
