<?php

namespace Drupal\Tests\ingredient\Kernel\Migrate\recipe61;

/**
 * Tests migration of Recipe 6.x-1.x ingredient variables to configuration.
 *
 * @group recipe
 */
class MigrateIngredientSettings61Test extends MigrateIngredient61TestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['ingredient'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(static::$modules);
    $this->executeMigrations(['recipe1x_ingredient_settings']);
  }

  /**
   * Tests migration of ingredient variables to configuration.
   */
  public function testMigration() {
    $config = \Drupal::config('ingredient.settings')->get();
    $this->assertSame(1, $config['ingredient_name_normalize']);
  }

}
