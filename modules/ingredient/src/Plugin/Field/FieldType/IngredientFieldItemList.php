<?php

namespace Drupal\ingredient\Plugin\Field\FieldType;

use Drupal\Core\Field\EntityReferenceFieldItemList;

/**
 * Represents a configurable entity ingredient field.
 */
class IngredientFieldItemList extends EntityReferenceFieldItemList {}
