<?php

namespace Drupal\ingredient\Plugin\migrate\destination;

use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;

/**
 * Provides the EntityIngredient class.
 *
 * @MigrateDestination(
 *   id = "entity:ingredient"
 * )
 */
class EntityIngredient extends EntityContentBase {}
